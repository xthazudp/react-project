const BASE_URL = process.env.REACT_APP_BASE_URL;
export const createOrder = (userId, token, createOrderData) => {
  return fetch(`${BASE_URL}/order/create/${userId}`, {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: `${token}`,
    },
    body: JSON.stringify({ order: createOrderData }),
  })
    .then((response) => {
      return response.json();
    })
    .catch((err) => {
      console.log(err);
    });
};
