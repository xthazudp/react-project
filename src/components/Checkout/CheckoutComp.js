import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { emptyCart } from './../Carts/CartHelper';
import { createOrder } from './CheckoutCompHelper';

const Checkout = (props) => {
  const [data, setData] = useState({
    loading: false,
    success: false,
    isEnable: true,
    error: '',
    address: '',
  });

  const userId =
    props.isLoggedIn && JSON.parse(localStorage.getItem('user'))._id;
  const token = props.isLoggedIn && localStorage.getItem('token');
  let deliveryAdd = data.address;

  const getTotal = () => {
    return props.products.reduce((currentValue, nextValue) => {
      return currentValue + nextValue.count * nextValue.price;
    }, 0);
  };

  const buy = () => {
    setData({ loading: true });
    // console.log('props values', props);
    const createOrderData = {
      products: props.products,
      amount: getTotal(props.products),
      address: deliveryAdd,
    };
    createOrder(userId, token, createOrderData)
      .then((response) => {
        emptyCart(() => {
          console.log('payment success and cart is empty');
          setData({
            loading: false,
            success: true,
            isEnable: false,
          });
        });
      })
      .catch((error) => {
        console.log('error payment', error);
        setData({ loading: false });
      });
  };
  const showError = (error) => {
    return (
      <div
        className='alert alert-danger'
        style={{ display: error ? '' : 'none' }}
      >
        {error}
      </div>
    );
  };

  const showSuccess = (success) => {
    return (
      <div
        className='alert alert-info'
        style={{ display: success ? '' : 'none' }}
      >
        Thanks! Your payment was successfull!
      </div>
    );
  };

  const showButton = (isEnable) => {
    return isEnable ? (
      <button onClick={buy} className='btn btn-success btn-block'>
        Pay
      </button>
    ) : (
      <button onClick={buy} className='btn btn-success btn-block' disabled>
        Pay
      </button>
    );
  };

  const showLoading = (loading) => loading && <h2>loading..</h2>;

  const handleAddress = (event) => {
    setData({ ...data, address: event.target.value });
  };

  let content = props.isLoggedIn ? (
    <div onBlur={() => setData({ ...data, error: '' })}>
      {props.products.length > 0 ? (
        <div>
          <div className='gorm-group mb-3'>
            <label className='text-muted'>Delivery Address:</label>
            <textarea
              onChange={handleAddress}
              className='form-control'
              value={data.address}
              placeholder='type your delivery address here..'
            ></textarea>
          </div>
          {showButton(data.isEnable)}
        </div>
      ) : null}
    </div>
  ) : getTotal() > 0 ? (
    <Link to='/'>
      {' '}
      <button className='btn btn-primary'>Sign in to checkout</button>
    </Link>
  ) : (
    ''
  );

  return (
    <div>
      <h4>Totals: Rs {getTotal()}</h4>
      {showLoading(data.loading)}
      {showSuccess(data.success)}
      {showError(data.error)}
      {content}
    </div>
  );
};

export default Checkout;
